# MatCont

A Matlab Toolbox for Numerical Bifurcation Analysis of continuous-time and smooth ODEs

MatCont is a Matlab software project for the numerical continuation and bifurcation study of continuous-time (ODE) and discrete-time (Map) parameterized dynamical systems. Leaders of the project are Willy Govaerts (Gent,B) and Yuri A. Kuznetsov (Utrecht,NL) and Hil G.E. Meijer (UT, Enschede, NL).

Publish a paper using our software? That's great. Please do us a favour and cite: New features of the software MatCont for bifurcation analysis of dynamical systems. A. Dhooge, and W. Govaerts, Yu.A. Kuznetsov, H.G.E. Meijer and B. Sautois, MCMDS 2008, Vol. 14, No. 2, pp 147-175, https://www.tandfonline.com/doi/full/10.1080/13873950701742754

In case you're stuck, use the forum, but to get a good answer provide:

What command do you give when this appears? Provide the exact steps (not all code, a simple example will suffice).
Indicate whether you use the GUI or CL version. Also Matlab-version and Operating System may help.
We hope you followed the tutorial(s)?
See MatContM for discrete-time maps.
